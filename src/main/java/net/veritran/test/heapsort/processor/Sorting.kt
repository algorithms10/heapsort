package net.maca.test.heapsort.processor

import java.util.*

class Sorting {

    companion object{

        fun heapSort(data: MutableList<Int>){
            var heapSize = data.size
            buildMaxHeap(data)
            for(i in data.size-1 downTo 1){
                var aux = data[0]
                data[0]= data[i]
                data[i]=aux
                heapSize--
                maxHeapify(data, 0, heapSize)
            }

        }

        private fun maxHeapify(data: MutableList<Int>, index: Int, heapSize:Int){
            val left = 2*index + 1
            val rigth = 2*index + 2
//            var largest: Int?
            var largest = if (left <= heapSize -1 && (data.get(left) > data.get(index))){
                left
            }else{
                index
            }

            if (rigth <= heapSize -1 && (data.get(rigth) > data.get(largest))) {
                largest = rigth
            }

            if (largest != index){
                val valueAux = data.get(index)
                data[index] = data.get(largest)
                data[largest]  = valueAux

                maxHeapify(data, largest, heapSize)

            }

        }

        private fun buildMaxHeap(data: MutableList<Int>){
            for ( i in ((data.size/2)-1) downTo 0){
                maxHeapify(data, i, data.size)
            }
        }



    }

}
package net.maca.test.heapsort

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import net.maca.test.heapsort.processor.Sorting
import java.util.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val values = Arrays.asList(16, 4,10,14,7,9,3,2,8,1)
        Toast.makeText(this, values.toString(), Toast.LENGTH_LONG).show()
        Log.d("Maca", values.toString())
        Sorting.heapSort(values)
        Log.d("Maca", values.toString())



    }


}